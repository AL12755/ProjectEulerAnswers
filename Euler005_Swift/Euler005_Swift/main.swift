//
//  main.swift
//  Euler005_Swift
//
//  Created by Adam Lyth on 23/06/2016.
//  Copyright © 2016 Adam Lyth. All rights reserved.
//

import Foundation

func checkRemainder(number: Int) -> Bool {
    for i in 1...20 {
        if number % i != 0 {
            return false
        }
    }
    return true
}

var number = 1

while checkRemainder(number) == false {
    number += 1
}

print(number)

