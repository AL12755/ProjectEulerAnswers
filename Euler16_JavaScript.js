// Naive approach of using an Array to store digits
// 2^1000 sum of digits.

var answer = [1]

for (var i=0; i< 1000; i++){
  //multiply all by 2
  for (var j = answer.length-1; j>=0; j--){
    answer[j] *= 2;
  }
  for (var j = answer.length; j>0; j--){
    while (answer[j] - 10 >= 0) {
      answer[j-1] += 1;
      answer[j] -= 10;
    }
  }
  if (answer[0] >= 10) {
    answer.unshift(0)
    while (answer[1] - 10 >= 0) {
      answer[0] += 1;
      answer[1] -= 10;
    }
  }
}

var sum = 0;
for (var i =0; i < answer.length ; i++){
  sum += answer[i]
}

console.log(sum)
