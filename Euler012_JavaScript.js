function primeFactors(num) {
  var factors = [];
  var i = 1;
  while (num != 1) {
    i += 1;
    if (num % i == 0) {
      num = num / i;
      factors.push(i)
      i -= 1;
    }
  }
  return factors
}

function divisors(pFactors, target) {
  var divisors = []
  var count = Array.apply(null, Array(pFactors.length)).map(Number.prototype.valueOf,0);
  var total = Math.pow(2 , pFactors.length);

  while (total != 0) {
    //Adjust
    for (var i=count.length;i>0;i--) {
      if (count[i] > 1) {
        count[i] = 0;
        count[i-1] += 1;
      }
    }
    //Count
    var num = 1
    for (var i =0; i< count.length ; i++){
      if (count[i] != 0){
        num *= pFactors[i];
      }
    }
    if (divisors.indexOf(num) == -1) {
      divisors.push(num)
    }
    //Increment
    count[count.length-1] += 1;
    total--;
  }
  return divisors
}

function triangleNumbers() {
  this.value = 1;
  this.index = 1;
  this.pFactors = [1];
  this.divisors = [1];
  this.increment = function() {
    this.index += 1;
    this.value += this.index;
    this.pFactors = primeFactors(this.value);
    this.divisors = divisors(this.pFactors,this.value);
  }
}

var sequence = new triangleNumbers()

while (sequence.divisors.length < 500){
  sequence.increment()
}
console.log(sequence.value)
