function primeFactors(num) {
  var factors = [];

  var i = 1;
  while (num != 1) {
    i += 1;
    if (num % i == 0) {
      num = num / i;
      factors.push(i)
      i -= 1;
    }
  }
  return factors
}

console.log(primeFactors(600851475143).pop())
