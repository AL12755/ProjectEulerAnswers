function decrypt(cipher, key){
  var plainText = []
  for (var i = 0; i < cipher.length; i+= key.length){
    for (var j = 0; j < key.length; j++){
      plainText.push(String.fromCharCode((cipher[i+j] ^ key[j]) % 128));
    }
  }
  return plainText.join("");
}

function checkWord(candidate){
  return candidate.indexOf("the") !== -1 && candidate.indexOf("and") !== -1 && candidate.indexOf("was") !== -1;
}

function sumValue(candidate, length) {
  var value = 0;
  var numArray = candidate.split("")

  var asciiVal = []
  for (var i = 0; i< length; i++){
    value += numArray[i].charCodeAt(0);
    asciiVal.push(numArray[i].charCodeAt(0))
  }
  console.log(value)
}


fs = require('fs');
var file = fs.readFileSync("/Users/adamlyth/Dropbox/Projects/ProjectEulerAnswers/Euler59_cipher.txt", 'utf-8').split(",").map(Number);

for (var i = 97; i < 123; i++){
  for (var j = 97; j < 123; j++){
    for (var k = 97; k < 123; k++){
      var candidate = decrypt(file,[i,j,k]);
      checkWord(candidate) && sumValue(candidate,file.length);
    }
  }
}
