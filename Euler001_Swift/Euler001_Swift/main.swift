//
//  main.swift
//  Euler001_Swift
//
//  Created by Adam Lyth on 23/06/2016.
//  Copyright © 2016 Adam Lyth. All rights reserved.
//

import Foundation

let start = 1
let end   = 1000

var count = start
var total = 0

while count < end {
    if count % 3 == 0 || count % 5 == 0 {
        total += count
    }
    count += 1
}

print(total)

