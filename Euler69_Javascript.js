function isPrime(value) {
    for(var i = 2; i <= Math.ceil(Math.sqrt(value)); i++) {
        if(value % i === 0) {
            return false;
        }
    }
    return value > 1;
}

var total = 1;
var i = 0;

while (total < 1000000){
  i++;
  while (isPrime(i) == false) {
    i++
  }
  total *= i;
}

console.log(total/i)
