=begin
The Fibonacci sequence is defined by the recurrence relation:

Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.
Hence the first 12 terms will be:

F1 = 1
F2 = 1
F3 = 2
F4 = 3
F5 = 5
F6 = 8
F7 = 13
F8 = 21
F9 = 34
F10 = 55
F11 = 89
F12 = 144
The 12th term, F12, is the first term to contain three digits.

What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
=end

class Fib
  def initialize
    @value = 1
    @last = 1
    @term = 2
  end

  def value
    @value
  end

  def index
    @term
  end

  def next
    temp = @value
    @value += @last
    @last = temp
    @term += 1
  end

  def digits
    return @value.to_s.size
  end
end



sequence = Fib.new

while sequence.digits < 1000 do
  sequence.next
end
puts sequence.index
