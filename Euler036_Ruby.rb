def isPalindrome?(x)
  return x.to_s == x.to_s.reverse
end

$target = 1000000

sum = 0
for i in 1...$target
  if isPalindrome?(i) and isPalindrome?(i.to_s(2).to_i)
    sum += i
  end
end

puts sum
