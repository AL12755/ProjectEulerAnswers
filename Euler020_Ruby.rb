def factorial(num)
  if num == 1
    return 1
  else
    return num * factorial(num-1)
  end
end

sum = 0
factorial(100).to_s.split("").each do |item|
  sum += item.to_i
end

puts sum
