//
//  main.swift
//  Euler007_Swift
//
//  Created by Adam Lyth on 23/06/2016.
//  Copyright © 2016 Adam Lyth. All rights reserved.
//

import Foundation

func isPrime(number: Int) -> Bool {
    let upper = Int(sqrt(Double(number))) + 1
    
    for i in 2...upper {
        if number % i == 0 {
            return false
        }
    }
    
    return true
}

var total = 1
var number = 1

while total < 10001 {
    number += 1
    if isPrime(number) {
        total += 1
    }
}

print(number)


