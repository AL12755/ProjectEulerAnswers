function node(i, j, value) {
    this.i = i;
    this.j = j;
    this.value = value;
    this.connections = []
    this.visited = false;
    this.added = false;
    this.distance = 100000000;

    this.connect = function(n) {
        this.connections.push(n)
    }
}

function getSmallestDistanceNode(queue) {
    var earliestValue = queue[0];
    var earliestPosition = 0;

    for (var i=1; i< queue.length; i++){
      if (queue[i].distance < earliestValue.distance){
        earliestValue = queue[i];
        earliestPosition = i;
      }
    }

    queue.splice(earliestPosition, 1);
    return earliestValue;
}

function resetDijkstra(graph) {
    for (var i = 0; i < graph.length; i++) {
        for (var j = 0; j < graph[i].length; j++) {
            graph[i][j].visited = false;
            graph[i][j].distance = 100000000;
            graph[i][j].added = false;
        }
    }
}

function dijkstra(startNode) {
    var queue = [startNode]
    startNode.distance = startNode.value;
    startNode.added = true;
    while (queue.length != 0) {
        var currentNode = getSmallestDistanceNode(queue)
        for (nodes of currentNode.connections) {
            //Add All nodes to the queue if they have not been visited
            if (nodes.added == false) {
              nodes.added = true;
              queue.push(nodes);
            }
            //If the distance to that node can be beaten by the last node plus its current distance then update it.
            if (nodes.visited == false && nodes.distance > currentNode.distance + nodes.value) nodes.distance = currentNode.distance + nodes.value;
        }
        currentNode.visited = true;
    }
}

function createMatrix() {
    var fs = require('fs');
    var file = fs.readFileSync('Euler82_Matrix.txt', 'utf-8').split(/\n/)
    file.pop()

    var matrix = []
    for (row of file) {
        matrix.push(row.split(",").map(Number))
    }
    return matrix
}

function createGraph() {
    var graph = []
    var matrix = createMatrix();

    //Create Nodes
    for (i in matrix) {
        var row = []
        for (j in matrix[i]) {
            row.push(new node(i, j, matrix[i][j]))
        }
        graph.push(row)
    }


    //Connect Nodes Up
    for (var i = graph.length - 1; i > 0; i--) {
        for (var j = 0; j < graph[i].length; j++) {
            graph[i][j].connect(graph[i - 1][j])
        }
    }

    //Connect Nodes Down
    for (var i = 0; i < graph.length - 1; i++) {
        for (var j = 0; j < graph[i].length; j++) {
            graph[i][j].connect(graph[i + 1][j])
        }
    }

    //Connect Nodes Right
    for (var i = 0; i < graph.length; i++) {
        for (var j = 0; j < graph[i].length - 1; j++) {
            graph[i][j].connect(graph[i][j + 1])
        }
    }
    return graph;
}

var graph = createGraph()
var minimumPath = 10000000;
for (var i = 0; i < graph.length; i++) {
    dijkstra(graph[i][0]);
    for (var j = 0; j < graph.length; j++) {
        if (graph[j][graph[j].length - 1].distance < minimumPath) minimumPath = graph[j][graph[j].length - 1].distance;
    }
    resetDijkstra(graph);
}

console.log(minimumPath)
