$target = 200
$coins  = [1,2,5,10,20,50,100,200]
$maxTargets = Array.new
$combinations = 0
$coins.each do |val|
  $maxTargets << $target/val
end

def iter(val, index)
  for i in 0..$maxTargets[index]
    if val + i*$coins[index] == $target
      $combinations += 1
      return
    elsif val + i*$coins[index] < $target
      if index != $coins.length-1
        iter(val + i*$coins[index],index+1)
      end
    else
      return
    end
  end
end

iter(0,0)
puts $combinations
