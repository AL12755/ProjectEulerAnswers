=begin
Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:

21 22 23 24 25
20  7  8  9 10
19  6  1  2 11
18  5  4  3 12
17 16 15 14 13

It can be verified that the sum of the numbers on the diagonals is 101.

What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?
=end

require 'matrix'

def lagrangePolynomial(datapoints)
  matrixSize = datapoints.length
  preMatrix = Array.new
  preAnswer = Array.new
  answer = Array.new

  for data in datapoints
    preAnswer << data[1]
  end

  for data in datapoints
    row = Array.new
    for i in 0...matrixSize
      row << data[0]**i
    end
    preMatrix << row
  end

  answerMatrix = Matrix.row_vector(preAnswer) * Matrix.columns(preMatrix).inverse
  answerMatrix.each_with_index do |e, row, col|
    answer << e.to_i
  end
  return answer
end

# print lagrangePolynomial([[1,1],[2,9],[3,25]])
# print lagrangePolynomial([[1,1],[2,3],[3,13]])
# print lagrangePolynomial([[1,1],[2,5],[3,17]])
# print lagrangePolynomial([[1,1],[2,7],[3,21]])

sum = 1
for i in 2...502
  sum += 4*i*i - 4*i + 1
  sum += 4*i*i - 10*i + 7
  sum += 4*i*i - 8*i + 5
  sum += 4*i*i - 6*i + 3
end

puts sum
