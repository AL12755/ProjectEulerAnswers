require 'prime'

def trunctable_prime?(p)
  val = p.to_s.split("")
  val.rotate!
  val.pop
  while !val.empty? do
    if !Prime.prime?(val.join.to_i)
      return false
    end
    val.rotate!
    val.pop
  end

  #right to left
  val = p.to_s.split("")
  val.pop
  while !val.empty? do
    if !Prime.prime?(val.join.to_i)
      return false
    end
    val.pop
  end

  return true
end

i = 0
trunctablePrimes = Array.new

$primes = Prime.each{ |p|
  next if p < 10
  break if i == 11
  if trunctable_prime?(p)
    i += 1
    trunctablePrimes << p
  end
}

puts trunctablePrimes.inject(0){|sum,x| sum + x }
