function numberMap(n){
  this.value = n;
  this.before = []
  this.after = []
}

var fs = require('fs');
var file = fs.readFileSync('Euler79_keypad.txt', 'utf-8').split(/\n/).map(Number)
file.pop();

for (item in file){
  file[item] = file[item].toString().split("").map(Number);
}

var numbers = []
for (var i = 0; i < 10; i++){
  numbers.push(new numberMap(i));
}

for (i in file){
  numbers[file[i][0]].after.push(file[i][1])
  numbers[file[i][0]].after.push(file[i][2])

  numbers[file[i][1]].before.push(file[i][0])
  numbers[file[i][1]].after.push(file[i][2])

  numbers[file[i][2]].before.push(file[i][0])
  numbers[file[i][2]].before.push(file[i][1])
}

for (item in numbers){
  numbers[item].after = Array.from(new Set(numbers[item].after));
  numbers[item].before = Array.from(new Set(numbers[item].before));
}

console.log(numbers)
