def isPalindrome?(value)
  return value.to_s == value.to_s.reverse
end

largest = 0
for x in 100...999
  for y in 100...999
    if x*y > largest and isPalindrome?(x*y)
      largest = x*y
    end
  end
end

puts largest
