function palindrome(str){
  return str == str.split('').reverse().join('');
}

function lychrelCandidate(initialValue){
  //Set initial values
  this.IV = initialValue;
  this.val = initialValue;
  this.itteration = 0;
  this.lynchrel = true;
  this.finished = false;

  this.complete = function() {
    while (this.finished == false) {
      this.next()
    }
  }

  //Increment
  this.next = function() {
    if (this.finished == false){
      var reverseNumber = parseInt(this.val.toString().split("").reverse().join(""))
      this.val = this.val + reverseNumber;
      this.itteration += 1;

      if (palindrome(this.val.toString())){
        this.lynchrel = false;
        this.finished = true;
      }
      if (this.itteration == 50){
        this.finished = true;
      }
    }
  }
}

var lynchrelNumbers = 0;
for (var i=1;i<10000;i++){
  var t = new lychrelCandidate(i)
  t.complete()
  if (t.lynchrel == true){
    lynchrelNumbers++
  }
}

console.log(lynchrelNumbers)
