function primeFactors(num) {
  var factors = [];
  var i = 1;
  while (num != 1) {
    i += 1;
    if (num % i == 0) {
      num = num / i;
      factors.push(i)
      i -= 1;
    }
  }
  return factors
}

function properDivisors(pFactors) {
  var divisors = []
  var count = Array.apply(null, Array(pFactors.length)).map(Number.prototype.valueOf,0);
  var total = Math.pow(2 , pFactors.length);

  while (total != 1) {
    //Adjust
    for (var i=count.length;i>0;i--) {
      if (count[i] > 1) {
        count[i] = 0;
        count[i-1] += 1;
      }
    }
    //Count
    var num = 1
    for (var i =0; i< count.length ; i++){
      if (count[i] != 0){
        num *= pFactors[i];
      }
    }
    if (divisors.indexOf(num) == -1) {
      divisors.push(num)
    }
    //Increment
    count[count.length-1] += 1;
    total--;
  }
  return divisors
}
