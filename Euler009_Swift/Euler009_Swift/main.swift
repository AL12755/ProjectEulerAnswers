//
//  main.swift
//  Euler009_Swift
//
//  Created by Adam Lyth on 23/06/2016.
//  Copyright © 2016 Adam Lyth. All rights reserved.
//

import Foundation

for b in 1...998 {
    for a in 1...b {
        let c = sqrt(Double(a*a+b*b))
        if Double(a)+Double(b)+c == 1000 {
            print(a*b*Int(c))
        }
    }
}

