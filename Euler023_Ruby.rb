=begin
A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.

A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.

As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.

Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
=end

require 'prime'

class Number
  attr_accessor :type

  def initialize(value)
    @value = value
    self.categorise
  end

  def value
    @value

  end

  def categorise
    sum = 0
    self.proper_divisors.each do |item|
      sum += item
    end

    if sum == @value
      self.type = 'perfect'
    elsif sum > @value
      self.type = 'abundant'
    elsif sum < @value
      self.type = 'deficient'
    end
  end


  def counterCheck(counter)
    len = counter.size
    for i in 0...len
      if counter[i] == 0
        return true
      end
    end
    return false
  end

  def incrementCounter(counter)
    len = counter.size
    counter[len-1] += 1
    (len-1).downto(0).each do |i|
      if counter[i] > 1
        counter[i-1] += 1
        counter[i] = 0
      end
    end
  end

  def proper_divisors
    primes = Prime.prime_division(@value)
    set = []
    for prime in primes
      for i in 0...prime[1]
        set << prime[0]
      end
    end
    proper_divisors = [1]

    counter = []
    len = set.length
    for i in 0...len
      counter << 0
    end

    while counterCheck(counter) do
      incrementCounter(counter)
      num = 1
      for i in 0...len
        if counter[i] == 1
          num *= set[i]
        end
      end
      if !proper_divisors.include? num and num != @value
        proper_divisors << num
      end
    end
    return proper_divisors
  end
end

aNumbers = Array.new
numbers = Array.new

for i in 1...28123+1 do
  numbers << 1
end

for i in 12...28123 do
  num = Number.new(i)
  if num.type == "abundant"
    aNumbers << num
  end
end

for first in aNumbers do
  for second in aNumbers do
    if first.value+second.value < 28123
      numbers[first.value+second.value-1] = 0
    end
  end
end

sum = 0
for i in 1...numbers.size do
  sum += i*numbers[i-1]
end

puts sum
