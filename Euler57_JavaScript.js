var math = require('mathjs');

math.config({
  number: 'BigNumber',  // Default type of number:
                        // 'number' (default), 'BigNumber', or 'Fraction'
  precision: 1000         // Number of significant digits for BigNumbers
});

function fraction(num,den){
  this.num = num
  this.den = den
}

function add(A,B){
  var ans = new fraction(math.add(math.multiply(A.num, B.den) , math.multiply(B.num,A.den)),math.multiply(A.den,B.den))
  return ans
}

function div(A,B){
  var ans = new fraction(math.multiply(A.num,B.den),math.multiply(A.den,B.num))
  return ans
}

function larger(A){
  if (A.num.e > A.den.e) {
    return 1
  } else {
    return 0
  }
}

var first = new fraction(math.bignumber(1),math.bignumber(1))
var second = new fraction(math.bignumber(1),math.bignumber(2))
var ans = add(first,second)
var tot = 0


for (var i=1; i < 1000; i++){
  tot += larger(ans)
  ans = add(first,div(first,add(first,ans)))
}
console.log(tot)



// console.log(mulArr([1,2],[6,2]))
// console.log(addArr([60,120],[50,40])) // 720 + 540
