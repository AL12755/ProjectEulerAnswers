function isPrime(value) {
    for(var i = 2; i <= Math.ceil(Math.sqrt(value)); i++) {
        if(value % i === 0) {
            return false;
        }
    }
    return value > 1;
}

function checkPairPrime(A,B){
   if (isPrime(parseInt(A + "" + B)) && isPrime(parseInt(B + "" + A))) return true;
   return false;
}

function Node(initialValue) {
  this.value = initialValue;
  this.degree = 1;
  this.connections = [initialValue];
  this.connect = function(value) {
    this.connections.push(value);
    this.degree++;
  }
}

function Graph(){
  this.Nodes = [];
  this.addNode = function (initialValue) {
    var newNode = new Node(initialValue);
    for (node in this.Nodes) {
      if (checkPairPrime(newNode.value,this.Nodes[node].value)) {
        newNode.connect(this.Nodes[node].value);
        this.Nodes[node].connect(newNode.value);
      }
    }
    this.Nodes.push(newNode);
  }
}

function counter(length, aim){
  this.count = Array.apply(null, Array(length)).map(Number.prototype.valueOf,0);
  this.aim = aim;
  this.next = function () {
    this.increment();
    while (this.sum() != this.aim){
      this.increment();
    }
  }
  this.sum = function () {
    var tot = 0;
    for (var i =0; i< this.count.length; i++){
      tot += this.count[i];
    }
    return tot;
  }
  this.increment = function () {
    this.count[this.count.length-1]++;
    this.reDistribute();
  }
  this.reDistribute = function () {
    for (var i = this.count.length; i> 0; i--){
      if (this.count[i] == 2){
        this.count[i-1]++;
        this.count[i] = 0;
      }
    }
  }
}

function choose(n, k) {
    if (k == 0) return 1;
    return (n * choose(n - 1, k - 1)) / k;
}

function permutationSet(searchNodes, permutation){
  var set = [];
  for (item in permutation){
    if (permutation[item]==1) set.push(searchNodes[item])
  }
  return set
}

function permutations(searchNodes, val){
  var permutations = [];
  var count = new counter(searchNodes.length, val)
  var items = choose(searchNodes.length, val);
  for (var i = 0; i<items; i++){
    count.next()
    permutations.push(permutationSet(searchNodes,count.count))
  }
  return permutations
}

function intersect(a, b) {
    var t;
    if (b.length > a.length) t = b, b = a, a = t; // indexOf to loop over shorter
    return a.filter(function (e) {
        if (b.indexOf(e) !== -1) return true;
    });
}

function compareCheckSets(checkSets, val){
  var start = checkSets[0].connections;
  for (var i = 1; i< checkSets.length; i++){
    start = intersect(start,checkSets[1].connections);
  }
   if (start.length >= val) {
     return true
   } else {
    return false;
  }
}

function searchCompleteGraph(primeGraph, val){
  var searchNodes = []
  for (node in primeGraph.Nodes){
    if (primeGraph.Nodes[node].degree >= val ){
      searchNodes.push(primeGraph.Nodes[node]);
    }
  }
  var checkSets = permutations(searchNodes, val)
  for (set in checkSets){
    if (compareCheckSets(checkSets[set],val) == true) {
      console.log(checkSets[set])
    }
  }
}

var i = 3;
var primeGraph = new Graph()

while (i < 700){
  console.log(i) 
  if (isPrime(i)) {
    primeGraph.addNode(i);
    searchCompleteGraph(primeGraph,4);
  }
  i++;
}
