function wordScore(word) {
  var total = 0;
  for (var i = 0; i < word.length; i++){
    total += word[i].charCodeAt() - 64
  }
  return total
}

var fs = require('fs');
var array = fs.readFileSync("/Users/adamlyth/Dropbox/Projects/ProjectEulerAnswers/Euler022_names.txt").toString().split(",");
for (item in array) {
  array[item] = array[item].substring(1,array[item].length - 1)
}
array.sort()

var score = 0;
for (var i = 0; i< array.length; i++){
  score += (i+1) * wordScore(array[i])
}


console.log(score)
