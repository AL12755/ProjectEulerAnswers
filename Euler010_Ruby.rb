require 'prime'
sum = 0
primes = Prime.take_while {|p| p < 2000000 }

for prime in primes
  sum += prime
end

puts sum
