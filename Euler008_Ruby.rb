file = "/Users/adamlyth/Dropbox/Projects/ProjectEulerAnswers/Euler008_Swift/grid.txt"
fileContents = IO.readlines(file)

$gridsize = 1000
$checksize = 13

grid = Array.new

for x in 0...$gridsize
    grid << fileContents[0][x].to_i
end

greatestProduct = 0

for x in 0...$gridsize-$checksize+1
  sum = 1
  for i in 0...$checksize
    sum = sum * grid[x+i]
  end
  if sum > greatestProduct
    greatestProduct = sum
  end
end

puts greatestProduct
