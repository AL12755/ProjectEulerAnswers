//
//  main.swift
//  Euler006_Swift
//
//  Created by Adam Lyth on 23/06/2016.
//  Copyright © 2016 Adam Lyth. All rights reserved.
//

import Foundation

var sumOfTheSquare = 0
var squareOfTheSum  = 0

for i in 1...100 {
    sumOfTheSquare += i*i
    squareOfTheSum += i
}

squareOfTheSum = squareOfTheSum*squareOfTheSum

print(squareOfTheSum-sumOfTheSquare)



