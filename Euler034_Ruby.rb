def factorial(value)
  if value == 1
    return 1
  else
    return value * factorial(value-1)
  end
end

def curious?(x)
  sum = 0
  x.to_s.split("").each do |item|
    sum += $factorials[item.to_i]
  end
  return sum == x
end

#Calculate the factorials prior
$factorials = [1]
for i in 1..9
  $factorials << factorial(i)
end


sum = 0
for i in 3..50000 do
  if curious?(i)
    sum += i
  end
end

puts sum
