function isPrime(value) {
    for(var i = 2; i < Math.ceil(Math.sqrt(value)); i++) {
        if(value % i === 0) {
            return false;
        }
    }
    return value > 1;
}

function primeSet(digits){
  const lower = Math.pow(10,digits-1) + 1
  const higher = Math.pow(10,digits)
  var primes = {}

  for (var i = lower; i< higher; i+= 2){
    if (isPrime(i)){
      primes[String(i)] = 1
    }
  }
  return primes
}

function replace(initialVal, A, replace){
  var val = initialVal.toString().split('').map(parseFloat);
  var ans = Array(val.length).join('0').split('').map(parseFloat)

  for (i in A){
    if (A[i] == 0){
      ans[i] = val[i]
    } else {
      ans[i] = replace
    }
  }
  return ans.join("")
}

function checkSets(initialVal){
  var stack = new incrementor(initialVal)
  stack.inc()

  var set = []

  while (stack.finished == false) {
    var row = []
    for (var i=0; i<10; i++){
      row.push(replace(initialVal,stack.arr,i))
    }
    set.push(row)
    stack.inc()
  }

  return set
}

function incrementor(initialVal) {
  this.value = initialVal;
  this.length = initialVal.toString().length
  this.arr = new Array(this.length+1).join('0').split('').map(parseFloat)
  this.incremented = 0;
  this.finished = false;

  this.inc = function() {
    if (this.finished == false) {
      this.arr[this.length-1]++;
      this.adjust();
      this.incremented++;
      if (this.incremented == Math.pow(2,this.length)-1){
        this.finished = true;
      }
    }
  }

  this.adjust = function() {
    for (var i=this.length - 1; i > 0; i--){
      if (this.arr[i] > 1) {
        this.arr[i] = 0;
        this.arr[i-1]++;
      }
    }
  }
}

function maxLength(val,primeSet){
  var checks = checkSets(val)
  var len = 0;
  var largestFamilyRow = []
  for (row in checks){
    var tempLen = 0;
    var tempRow = []
    for (item in checks[row]){
      if (primeSet[checks[row][item].toString()] == 1) {
        tempLen++
        tempRow.push(checks[row][item])
      }
    }
    if (tempLen > len) {
      len = tempLen
      largestFamilyRow = tempRow
    }
  }
  return [len,largestFamilyRow]
}


for (var p=5; p < 10; p++){
  var primes = primeSet(p)
  for (prime in primes){
    if (prime > 56003){
      var ans = maxLength(prime,primes)
      if (ans[0] == 8 && ans[1].indexOf(prime) > -1){
        console.log(prime)
        return 0
      }
    }
  }
}

// console.log(maxLength(56003,primeSet(5)))
