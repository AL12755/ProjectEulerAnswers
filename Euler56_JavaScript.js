
//A is an array and b is a number
function mul(a,b){
  //Multiply all values
  for (var i=0; i< a.length; i++){
    a[i] *= b
  }

  //Cascade all other values
  for(var i=a.length-1; i>=0; i--){
    while (a[i] >= 10){
      a[i] -=10;
      if (i == 0) {
        a.unshift(1)
        i++
      } else {
        a[i-1] += 1;
      }
    }
  }
  return a
}

function pow(a,b){
  var num = a.toString(10).split("").map(function(t){return parseInt(t)})
  for (var i = 1; i< b; i++){
    num = mul(num,a)
  }
  return num
}

function digitalSum(a){
  var sum = 0;
  for (var i=0; i< a.length; i++){
    sum += a[i]
  }
  return sum
}

var maxDigitalSum = 0;
for (var x=1; x< 100; x++){
  for (var y=1; y<100; y++){
    var tot = digitalSum(pow(x,y));
    if (tot> maxDigitalSum) {
      maxDigitalSum = tot;
    }
  }
}
console.log(maxDigitalSum)
