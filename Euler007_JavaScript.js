function isPrime(num) {
  if (num < 2) return false;
  var q = Math.floor(Math.sqrt(num))
  for (var i=2;i<=q;i++) if (num%i==0) return false;
  return true;
}

primes = 0
checkVal = 1
while (primes <= 10000
) {
  checkVal += 1
  if (isPrime(checkVal)) primes += 1
}

console.log(checkVal)
