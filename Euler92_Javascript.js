function totalSum(val){
  var temp = val.toString().split("").map(Number)
  var tot = 0;
  for (item of temp){
    tot += item*item
  }
  return tot
}

var summedItems = {}
for (var i=1; i< 10000000; i++){
  var temp = totalSum(i)
  summedItems[temp] = (summedItems[temp] || 0) + 1;;
}

while (Object.keys(summedItems).length != 2) {
  for (var key in summedItems) {
    var temp = totalSum(key)
    if (key != 1 && key != 89){
      summedItems[temp] = (summedItems[temp] || 0) + summedItems[key];;
      delete summedItems[key];
    }
  }
  console.log(summedItems)
}
console.log(summedItems)
