function range(n) {
    var upper = Math.pow(10, n) - 1;
    var lower = Math.pow(10, n - 1);
    return [lower, upper]
}

function numberRange(n) {
    var upperLower = range(n)
    upperLower[0] = Math.ceil(Math.pow(upperLower[0], 1 / n))
    upperLower[1] = Math.floor(Math.pow(upperLower[1], 1 / n))
    return upperLower
}

var n = 1;
var total = 0;

do {
    upperLower = numberRange(n);
    for (var i = upperLower[0]; i <= upperLower[1] && i != 10; i++) {
        total++;
    }
    n++;
} while (upperLower[0] != 10)

console.log(total)
