filename = "/Users/adamlyth/Dropbox/Projects/ProjectEulerAnswers/Euler022_names.txt"
fileContents = IO.readlines(filename)
names = fileContents[0].tr("\"","").split(",")
names = names.sort

def value(name)
  sum = 0
  name.split("").each do |char|
    sum += char.ord - 64
  end
  return sum
end

sum = 0
for i in 0...names.length
  sum += value(names[i]) * (i+1)
end

puts sum
