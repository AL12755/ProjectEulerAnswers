function collatzSequence(start) {
  this.start = start;
  this.value = start;
  this.ended = false;
  this.length = 1;

  this.next = function() {
    if (this.ended == false){
      if (this.value % 2 == 0) {
        this.value = this.value/2;
      } else {
        this.value = 3*this.value + 1;
      }
      this.length++;
      if (this.value == 1) {
        this.ended = true;
      }
    }
  }

  this.finish = function() {
    while (this.ended != true) {
      this.next();
    }
  }
}

var max = 0;
var sNumber = 0
for (var i = 1; i< 1000000;i++){
  var sequence = new collatzSequence(i);
  sequence.finish();
  if (sequence.length > max) {
    max = sequence.length;
    sNumber = i;
  }
}

console.log(sNumber)
