def checkPanDigital(val)
  #Check 1-digit * 4-digit
  x = val[0].to_i
  y = val[1].to_i*1000 + val[2].to_i*100 + val[3].to_i*10 + val[4].to_i
  z = val[5].to_i*1000 + val[6].to_i*100 + val[7].to_i*10 + val[8].to_i
  if x*y == z
    $configs << z
  end

  #Check 2-digit * 3-digit
  x = val[0].to_i*10 + val[1].to_i
  y = val[2].to_i*100 + val[3].to_i*10 + val[4].to_i
  if x*y == z
    $configs << z
  end
end

def arraySum(values)
  sum = 0
  values.each do |item|
    sum += item
  end
  sum
end

$configs = Array.new
a = [1,2,3,4,5,6,7,8,9]
a = a.permutation.to_a
a.each do |config|
  checkPanDigital(config)
end

puts arraySum($configs.uniq)
