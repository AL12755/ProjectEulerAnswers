require 'prime'

class TriangleNumberList
  def initialize
    @@number = 1
    @@value = 1
    @@divs = 1
  end

  def factors_of
    number = @@value
    primes, powers = number.prime_division.transpose
    exponents = powers.map{|i| (0..i).to_a}
    divisors = exponents.shift.product(*exponents).map do |powers|
      primes.zip(powers).map{|prime, power| prime ** power}.inject(:*)
    end
    @@divs = divisors.sort.map{|div| [div, number / div]}.size
  end

  def next
    @@number += 1
    @@value += @@number
    self.factors_of
  end
  def value
    return @@value
  end
  def divs
    return @@divs
  end
end

tN = TriangleNumberList.new

while tN.divs < 500 do
  tN.next
end

puts tN.value
