require 'prime'

def circularPrime?(x)
  perm = x.to_s.split("")
  for i in 0...perm.size do
    if !$primes.include? perm.rotate(i).join.to_i
      return false
    end
  end
  return true
end

def perfectlyOdd
  set = Array.new
  $primes.each do |x|
    add = true
    values = x.to_s.split("")
    values.each do |val|
      if val.to_i % 2 == 0 or val.to_i == 5
        add = false
      end
    end
    if add
      set << x
    end
  end
  return set
end

$target = 1000000
$primes = Prime.take_while {|p| p < $target}
$primes = perfectlyOdd

circularPrimes = [2,5]
$primes.each do |prime|
  if circularPrime?(prime)
    circularPrimes << prime
  end
end


puts circularPrimes.length
