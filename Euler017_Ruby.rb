singles = ["one","two","three","four","five","six","seven","eight","nine"]
doubles = ["ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"]
wholes = ["twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"]

letters = 0

for item in singles
  letters += item.length
end

for item in doubles
  letters += item.length
end

for item in wholes
  letters += item.length
end

for item2 in wholes
  for item3 in singles
    letters += item2.length + item3.length
  end
end

for item7 in singles
  letters += item7.length + "hundred".length


  for item in singles
    letters += item.length + "hundredand".length + item7.length
  end

  for item in doubles
    letters += item.length + "hundredand".length + item7.length
  end

  for item in wholes
    letters += item.length + "hundredand".length + item7.length
  end

  for item2 in wholes
    for item3 in singles
      letters += item2.length + item3.length + "hundredand".length + item7.length
    end
  end
end

letters += "onethousand".length

puts letters
