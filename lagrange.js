var math = require('mathjs');

module.exports = {
  lagrange: function(coOrdinates){
    return math.multiply(math.inv(findVandermonde(coOrdinates)),findY(coOrdinates));
  }
};


var findY = function(coOrdinates){
  var vector = []

  for (var i =0; i< coOrdinates.length; i++){
    vector.push(coOrdinates[i][1]);
  }
  return vector
}

var findVandermonde = function(coOrdinates) {
  var depth = coOrdinates.length;
  var Matrix = []

  for (var i = 0; i< depth; i++){
    var row = []
    for (var j = 0; j< depth; j++) {
      row.push(Math.pow(coOrdinates[i][0],j))
    }
    Matrix.push(row)
  }
  return Matrix
}
