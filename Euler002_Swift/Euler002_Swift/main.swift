//
//  main.swift
//  Euler002_Swift
//
//  Created by Adam Lyth on 23/06/2016.
//  Copyright © 2016 Adam Lyth. All rights reserved.
//

import Foundation

let end = 4000000

var previous = 1
var pointer  = 1
var total = 0

func updateFibonnaciSequence(previous: Int, pointer: Int) -> (Int, Int) {
    return(pointer, previous+pointer)
}

while pointer < end {
    if pointer % 2 == 0 {
        total += pointer
    }
    (previous,pointer) = updateFibonnaciSequence(previous, pointer: pointer)
}

print(total)

