function Comparator(a, b) {
  if (a[0] < b[0]) return -1;
  if (a[0] > b[0]) return 1;
  return 0;
}

function Comparator2(a, b) {
  if (a < b) return 1;
  if (a > b) return -1;
  return 0;
}

function sortedValue(Hand){
  var temp = []
  for (var i = 0; i < Hand.length; i++) {
    temp.push(Hand[i][0])
  }
  temp = temp.sort(Comparator2)
  return temp
}

function switchAce(Hands){
  var temp = [];
  for (hand in Hands){
    temp.push(Hands[hand].slice(0))
  }
  for (card in temp){
    if(temp[card][0] == 14){
      temp[card][0] = 1
    }
  }
  return temp
}

function checkPair(Hand){
  counts = {}
  for (card in Hand){
    counts[Hand[card][0]] = (counts[Hand[card][0]] || 0) + 1;
  }

  for (value in counts){
    if (counts[value] == 2) {
      return [parseInt(value)].concat(sortedValue(Hand))
    }
  }
  return 0
}

function checkTwoPair(Hand){
  counts = {}
  for (card in Hand){
    counts[Hand[card][0]] = (counts[Hand[card][0]] || 0) + 1;
  }
  var tot = 0
  var val = []
  for (value in counts){
    if (counts[value] == 2) {
      val.push(parseInt(value));
      tot++;
    }
  }
  val = val.sort(Comparator2)

  if (tot == 2){
    return val.concat(sortedValue(Hand))
  }
  return false
}

function checkThreeKind(Hand){
  counts = {}
  for (card in Hand){
    counts[Hand[card][0]] = (counts[Hand[card][0]] || 0) + 1;
  }
  for (value in counts){
    if (counts[value] == 3) {
      return [parseInt(value)].concat(sortedValue(Hand))
    }
  }
  return false
}

function checkStraight(Hand){
  Hand = Hand.sort(Comparator)
  for (var i=0; i< Hand.length-1; i++){
    if (Hand[i][0] + 1 != Hand[i+1][0]){
      return false;
    }
  }
  // console.log(sortedValue(Hand))
  return sortedValue(Hand)
}

function checkFlush(Hand) {
  var type = Hand[0][1]
  for (var i =1; i < Hand.length; i++){
    if (Hand[i][1] != type) {
      return false;
    }
  }
  return sortedValue(Hand)
}

function checkFullHouse(Hand){
  counts = {}
  for (card in Hand){
    counts[Hand[card][0]] = (counts[Hand[card][0]] || 0) + 1;
  }

  var tot = 0
  for (value in counts){
    tot++;
  }

  //Exists a Full House
  if (tot == 2){
    var ret = [0,0];
    for (value in counts){
      if (counts[value] == 3){
        ret[0] = parseInt(value);
      } else {
        ret[1] = parseInt(value);
      }
    }
    return ret;
  }
  return 0
}

function checkFourKind(Hand){
  counts = {}
  for (card in Hand){
    counts[Hand[card][0]] = (counts[Hand[card][0]] || 0) + 1;
  }
  for (value in counts){
    if (counts[value] == 4) {
      return sortedValue(Hand)
    }
  }
  return 0
}

function checkStraightFlush(Hand){
  if(checkFlush(Hand) != 0 && checkStraight(Hand) != 0){
    return sortedValue(Hand)
  }
  return false
}

function checkRoyalFlush(Hand) {
  Hand = Hand.sort(Comparator)
  if(checkFlush(Hand) != 0 && checkStraight(Hand) != 0 && Hand[Hand.length-1][0]==14){
    return sortedValue(Hand)
  }
  return 0
}

function convert(Hands) {
  const conversion = {"2":2,
                    "3":3,
                    "4":4,
                    "5":5,
                    "6":6,
                    "7":7,
                    "8":8,
                    "9":9,
                    "T":10,
                    "J":11,
                    "Q":12,
                    "K":13,
                    "A":14}
  for (Hand in Hands){
    for (var i = 0; i < 2; i++){
      for (card in Hands[Hand][i]){
        Hands[Hand][i][card][0] = conversion[Hands[Hand][i][card][0]]
      }
    }
  }
  return Hands
}

function handValue(Hand){
  var handVal = []
  var temp = switchAce(Hand)
  if (checkRoyalFlush(Hand) != 0) {
    handVal.push(14)
    return handVal
  } else if (checkStraightFlush(Hand) != 0){
    handVal.push(13)
    handVal = handVal.concat(checkStraightFlush(Hand))
    return handVal
  } else if (checkFourKind(Hand) != 0){
    handVal.push(12)
    handVal = handVal.concat(checkFourKind(Hand))
    return handVal
  } else if (checkFullHouse(Hand) != 0){
    handVal.push(11)
    handVal = handVal.concat(checkFullHouse(Hand))
    return handVal
  } else if (checkFlush(Hand) != 0){
    handVal.push(10)
    handVal = handVal.concat(checkFlush(Hand))
    return handVal
  } else if (checkStraight(Hand) != 0){
    handVal.push(9)
    handVal = handVal.concat(checkStraight(Hand))
    return handVal
  } else if (checkStraight(temp) != 0){
    handVal.push(9)
    handVal = handVal.concat(checkStraight(temp))
    return handVal
  } else if (checkThreeKind(Hand) != 0){
    handVal.push(8)
    handVal = handVal.concat(checkThreeKind(Hand))
    return handVal
  } else if (checkTwoPair(Hand) != 0){
    handVal.push(7)
    handVal = handVal.concat(checkTwoPair(Hand))
    return handVal
  } else if (checkPair(Hand) != 0){
    handVal.push(6)
    handVal = handVal.concat(checkPair(Hand))
    return handVal
  } else {
    handVal.push(5)
    handVal = handVal.concat(sortedValue(Hand))
    return handVal
  }
}

function readFromFile() {
  var fs = require('fs');
  var file = fs.readFileSync('/Users/adamlyth/Dropbox/Projects/ProjectEulerAnswers/Euler54_poker.txt', 'utf-8').split(/\n/);
  var Hands = []
  for (var i =0; i<file.length-1; i++){
    var tempHands = file[i].split(" ")
    var player1 = new Array()
    var player2 = new Array()
    for (j=0;j<5;j++){
      player1.push(tempHands[j].split(""))
    }
    for (j=5;j<10;j++){
      player2.push(tempHands[j].split(""))
    }
    Hands.push([player1,player2])
  }
  return convert(Hands)
}

function compareHands(player1, player2) {
  console.log(player1, player2)
  for (var i=0; i<player1.length && i<player2.length; i++){
    if (player1[i] > player2[i]) {
      return 1
    } else if (player1[i] < player2[i]) {
      return 0
    }
  }
}

function winner(Hands) {
  var player1 = handValue(Hands[0])
  var player2 = handValue(Hands[1])
  // console.log(player1)
  // console.log(player2)
  // console.log(compareHands(player1,player2))
  // console.log(player1 + " //// " + player2 + " //// " + compareHands(player1,player2) + " //// " + Hands[0] + " //// " + Hands[1])
  return compareHands(player1,player2)
}

function solve() {
  //Setup Hands in an array
  Hands = readFromFile()
  var totalPlayer1 = 0;
  for (Hand in Hands){
    totalPlayer1 += winner(Hands[Hand])
  }
  console.log(totalPlayer1)
}

solve()
// console.log(sortedValue([[7,3],[8,4],[1,7],[16,42]]))
