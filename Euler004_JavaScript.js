function reverse(s){
    return s.split("").reverse().join("");
}

function isPalindrome(i,j){
  var val = (i*j).toString();
  if (val == reverse(val)) {
    return true;
  } else {
    return false;
  }
}

var largest = 0;

for (i=100;i<1000;i++){
  for (j=100;j<1000;j++){
    if (isPalindrome(i,j) && i*j > largest) {
      largest = i*j;
    }
  }
}

console.log(largest)
