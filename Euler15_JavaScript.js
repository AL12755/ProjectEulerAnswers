function Node(i, j) {
    this.i = i;
    this.j = j;
    this.distance = 0;
    this.distanceset = false;
    this.right = null;
    this.down = null;
}

function routes(Node) {
    if (Node.distanceset == true) {
        return Node.distance;
    }

    Node.distance = routes(Node.down) + routes(Node.right)
    Node.distanceset = true;
    return Node.distance;
}

function solve() {
    //Create Shell Graph
    var Graph = new Array()
    for (var x = 0; x < width; x++) {
        var row = new Array()
        for (var y = 0; y < height; y++) {
            row.push(new Node(x, y))
        }
        Graph.push(row)
    }

    //Connect Graph
    for (var x = 0; x < width - 1; x++) {
        for (var y = 0; y < height - 1; y++) {
            Graph[x][y].right = Graph[x + 1][y];
            Graph[x][y].down = Graph[x][y + 1];
        }
    }

    for (var x = 0; x < width - 1; x++) {
        Graph[x][height - 1].distance = 1;
        Graph[x][height - 1].distanceset = true;
    }
    for (var y = 0; y < height - 1; y++) {
        Graph[width - 1][y].distance = 1;
        Graph[width - 1][y].distanceset = true;
    }

    console.log(routes(Graph[0][0]))
}

const height = 21
const width = 21
solve()
