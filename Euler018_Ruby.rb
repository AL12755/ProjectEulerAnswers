class Node
  attr_accessor :right, :left, :distance, :distanceset
  @value = 0
  @row = 0
  @column = 0

  def initialize(value,row,column)
    @value = value
    @row = row
    @column = column
  end

  def value
    @value
  end

  def row
    @row
  end

  def column
    @column
  end
end

def min(x, y)
  if x< y
    return x
  else
    return y
  end
end

def max(x, y)
  if x< y
    return y
  else
    return x
  end
end

def graphdistance(node)
  if node.distanceset
    return node.distance
  else
    node.distance = node.value + max(graphdistance(node.left), graphdistance(node.right))
    node.distanceset = true
    return node.distance
  end
end

filename = "/Users/adamlyth/Dropbox/Projects/ProjectEulerAnswers/Euler018_grid.txt"
fileContents = IO.readlines(filename)

graph = Array.new
rn = 0
cn = 0

for line in fileContents
  row = Array.new
  cn = 0
  line.split(" ").each do |item|
    row << Node.new(item.to_i,rn,cn)
    cn += 1
  end
  graph << row
  rn += 1
end

size = graph.size - 1

for row in graph
  for item in row
    if item.row < size
      item.left = graph[item.row+1][item.column]
      item.right = graph[item.row+1][item.column+1]
    else
      item.left = nil
      item.right = nil
    end
  end
end

for row in graph
  for item in row
    if item.row == size
      item.distance = item.value
      item.distanceset = true
    else
      item.distance = 0
      item.distanceset = false
    end
  end
end

puts graphdistance(graph[0][0])
