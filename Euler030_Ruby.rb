def sumDigits(x)
  sum = 0
  x.to_s.split("").each do |val|
    sum += val.to_i**$power
  end
  sum
end

def sumArray(x)
  sum = 0
  x.each do |val|
    sum += val
  end
  sum
end

$power = 5
$maxValue = $power*9**$power

values = Array.new
i = 2

while i <= $maxValue do
  if sumDigits(i) == i
    values << i
  end
  i += 1
end

print sumArray(values)
