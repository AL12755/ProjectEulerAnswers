require 'prime'

def equaldistance(x)
  x.each do |i|
    x.each do |j|
      x.each do |k|
        if i != j and j!= k
          row = [i,j,k].sort
          if row[1] - row[0] == row[2] - row[1]
            return [i,j,k]
          end
        end
      end
    end
  end
  return false
end

#Generates all 4digit primes
primes123digits = Prime.take_while{ |p| p.to_s.length < 4}
primes1234digits = Prime.take_while{ |p| p.to_s.length < 5}
primes = primes1234digits - primes123digits

#Creates a blank array
permutationPrimes = Array.new
#For all 4 digit primes
primes.each do |prime|
  #create a new Array
  row = Array.new
  #Find all permutations of the prime
  perms = prime.to_s.split("").permutation

  #for all permutations
  perms.each do |perm|
    #If the permutation is a 4 digit prime
    if primes.include? perm.join.to_i
      #Add if to the row
      row << perm.join.to_i
      #delete it from the array

      primes.delete(perm.join.to_i)
    end
  end
  if row.length > 1
    permutationPrimes << row
  end
end

permutationPrimes.each do |val|
  if val.length >= 3
    if equaldistance(val)
      print equaldistance(val)
    end
  end
end
