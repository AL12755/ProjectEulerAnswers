function Fibonnaci(){
  this.current = 1
  this.previous = 1
  this.index = 2
  this.increment = function() {
    var temp = this.current;
    this.current = this.previous + this.current;
    this.previous = temp;
    this.index += 1;
  }
}

var sequence = new Fibonnaci();
var sum = 0;

while (sequence.current < 4000000) {
  sequence.increment();
  if (sequence.current % 2 == 0) sum += sequence.current;
}

console.log(sum);
