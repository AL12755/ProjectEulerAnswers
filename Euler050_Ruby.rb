require 'prime'

target = 1_000_000
$primes = Prime.take_while {|p| p < target}

consecutiveSums = Array.new

for i in 0...$primes.length do
  row = []
  j = i
  sum = $primes[j]
  while sum < target and j < $primes.length - 1 do
    row << sum
    j += 1
    sum += $primes[j]
  end
  consecutiveSums << row
end

largestChain = 21
largestChainPrime = 953

consecutiveSums.each do |val|
  if val.length > largestChain
    for i in largestChain-1...val.length
      if Prime.prime?(val[i])
        largestChain = i+1
        largestChainPrime = val[i]
      end
    end
  end
end

p largestChainPrime
