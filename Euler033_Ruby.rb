numProd = 1
denomProd = 1

for x0 in 10..99
  for y0 in 10..99
    numerator = x0.to_s.split("")
    denominator = y0.to_s.split("")
    intersection = numerator & denominator
    numerator = numerator - intersection
    denominator = denominator - intersection
    x1 = numerator.join.to_f
    y1 = denominator.join.to_f
    if intersection.length > 0 and x0.to_f/y0.to_f == x1/y1 and x0 % 10 != 0 and y0 % 10 != 0 and x0 < y0
      # puts "#{x0.to_f}/#{y0.to_f} = #{x1}/#{y1}"
      numProd *= x1
      denomProd *= y1
    end
  end
end

puts denomProd/numProd
