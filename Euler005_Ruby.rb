require 'prime'

def factors(value)
  return Prime.prime_division(value)
end

def smallestProduct(product)
  vals = Array.new
  pFactors = Array.new(product,0)

  (2..product).each do |i|
    for fact in factors(i)
      vals << fact
    end
  end

  for val in vals
    if val[1] > pFactors[val[0]-1]
      pFactors[val[0]-1] = val[1]
    end
  end

  total = 1

  pFactors.each_with_index do |key,value|
    while key > 0
      total = total * (value+1)
      key += -1
    end
  end

  return total
end

print smallestProduct(20)
