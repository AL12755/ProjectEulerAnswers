function isPrime(num) {
  if (num < 2) return false;
  var q = Math.floor(Math.sqrt(num));
  for (var i=2;i<=q;i++) if (num%i==0) return false;
  return true;
}

var primes = 0;

for (var i = 2; i < 2000000; i++) {
  if (isPrime(i)) primes += i;
}

console.log(primes);
