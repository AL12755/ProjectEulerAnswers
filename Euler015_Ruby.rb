class Node
  attr_accessor :right, :down, :distance, :distanceset
  @xval = 0
  @yval = 0

  def initialize(x,y)
    @xval = x
    @yval = y
  end

  def x
    return @xval
  end

  def y
    return @yval
  end
end

def distance(node)
  if node.distanceset == true
    return node.distance
  end

  node.distance = distance(node.right) + distance(node.down)
  node.distanceset = true
  return node.distance
end


graph = Array.new
for y in 0...21
  row = Array.new
  for x in 0...21
    row << Node.new(x,y)
  end
  graph << row
end


for y in 0...21
  for x in 0...21
    if x == 20
      graph[x][y].right = nil
    else
      graph[x][y].right = graph[x+1][y]
    end
  end
end


for y in 0...21
  for x in 0...21
    if y == 20
      graph[x][y].down = nil
    else
      graph[x][y].down = graph[x][y+1]
    end
  end
end

for y in 0...21
  for x in 0...21
    graph[x][y].distanceset = false
  end
end

for x in 0...21
  graph[x][20].distanceset = true
  graph[x][20].distance = 1
end

for y in 0...21
  graph[20][y].distanceset = true
  graph[20][y].distance = 1
end


puts distance(graph[0][0])
