function primeFactors(num) {
  var factors = [];
  var i = 1;
  while (num != 1) {
    i += 1;
    if (num % i == 0) {
      num = num / i;
      factors.push(i)
      i -= 1;
    }
  }
  return factors
}

function distribute(factors,counter){
  var tempCounter = Array.apply(null, Array(20)).map(Number.prototype.valueOf,0);

  for (j=0;j<factors.length;j++){
    tempCounter[factors[j]-1] += 1;
  }
  for (j=0;j<counter.length;j++){
    if (tempCounter[j] > counter[j]) counter[j] = tempCounter[j];
  }
  return counter;
}

function sumDistribution(counter){
  total = 1;
  for (j=0;j<counter.length;j++){
    total *= Math.pow(j+1,counter[j]);
  }
  return total;
}

var counter = Array.apply(null, Array(20)).map(Number.prototype.valueOf,0);
for (i=1;i<=20;i++){
  counter = distribute(primeFactors(i),counter);
}

console.log(sumDistribution(counter));
