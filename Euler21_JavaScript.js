function primeFactors(num) {
  var factors = [];
  var i = 1;
  while (num != 1) {
    i += 1;
    if (num % i == 0) {
      num = num / i;
      factors.push(i)
      i -= 1;
    }
  }
  return factors
}

function properDivisors(pFactors) {
  var divisors = []
  var count = Array.apply(null, Array(pFactors.length)).map(Number.prototype.valueOf,0);
  var total = Math.pow(2 , pFactors.length);

  while (total != 1) {
    //Adjust
    for (var i=count.length;i>0;i--) {
      if (count[i] > 1) {
        count[i] = 0;
        count[i-1] += 1;
      }
    }
    //Count
    var num = 1
    for (var i =0; i< count.length ; i++){
      if (count[i] != 0){
        num *= pFactors[i];
      }
    }
    if (divisors.indexOf(num) == -1) {
      divisors.push(num)
    }
    //Increment
    count[count.length-1] += 1;
    total--;
  }
  return divisors
}

function sumArray(A) {
  var total = 0;
  for (item in A){
    total += A[item];
  }
  return total;
}

function amicableNumber(value) {
  this.value = value;
  this.AM = null;
  this.pointAM = null;
  this.pair = function() {
    var C = sumArray(properDivisors(primeFactors(this.value)))
    if (C < 10000) {
      this.AM = C
    }
  }

}

var numbers = [0];

//Setup graph
for (var i = 1; i < 10000; i++){
  numbers.push(new amicableNumber(i))
  numbers[i].pair()
}

//Setup pointers between them
for (var i = 1; i < 10000; i++){
  if (numbers[i].AM != null) {
    numbers[i].pointAM = numbers[numbers[i].AM]
  }
}

//count pointers
var pair = 0;
for (var i = 1; i < 10000; i++){
  if (numbers[i].pointAM != null && numbers[i].pointAM.pointAM != null) {
    // console.log(numbers[i].value + " " + numbers[i].pointAM.value + " " + numbers[i].pointAM.pointAM.value)
    if (numbers[i].value == numbers[i].pointAM.pointAM.value && numbers[i].value != numbers[i].pointAM.value) {
      console.log(numbers[i].value)
      pair += numbers[i].value;
    }
  }
}

console.log(pair)
