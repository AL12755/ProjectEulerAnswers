var grid = "\
75 \
95 64 \
17 47 82 \
18 35 87 10 \
20 04 82 47 65 \
19 01 23 75 03 34 \
88 02 77 73 07 63 67 \
99 65 04 28 06 16 70 92 \
41 41 26 56 83 40 80 70 33 \
41 48 72 33 47 32 37 16 94 29 \
53 71 44 65 25 43 91 52 97 51 14 \
70 11 33 28 77 73 17 78 39 68 17 57 \
91 71 52 38 17 14 91 43 58 50 27 29 48 \
63 66 04 68 89 53 67 30 73 16 69 87 40 31 \
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23".split(" ").map(Number)

function Node(value, x, y) {
  this.value = value;
  this.x = x;
  this.y = y;
  this.down = null;
  this.right = null;
  this.checkedValue = 0;
  this.checked = false;
}

function sum(x){
  if (x == 1 || x == 0){
    return x;
  } else {
    return x + sum(x-1);
  }
}

function MaxDistance(Node) {
  if (Node.checked == true){
    return Node.checkedValue
  }
  Node.checkedValue = Node.value + Math.max(MaxDistance(Node.down),MaxDistance(Node.right));
  Node.checked = true;

  return Node.checkedValue;
}

const rows = 15;
var Graph = new Array()

for (var height = 1 ; height <= rows; height++){
  var Row = new Array()
  for (var width = 0; width < height; width ++) {
    Row.push(new Node(grid[sum(height-1)+width],width,height-1))
  }
  Graph.push(Row)
}
for (var height = 0 ; height < rows-1; height++){
  for (var width = 0; width <= height; width ++) {
    Graph[height][width].down = Graph[height+1][width]
    Graph[height][width].right = Graph[height+1][width+1]
  }
}

for (var width = 0; width < 15; width ++) {
  Graph[14][width].checked = true;
  Graph[14][width].checkedValue = Graph[14][width].value;
}
// console.log(Graph)
console.log(MaxDistance(Graph[0][0]))
