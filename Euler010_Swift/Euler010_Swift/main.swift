//
//  main.swift
//  Euler010_Swift
//
//  Created by Adam Lyth on 23/06/2016.
//  Copyright © 2016 Adam Lyth. All rights reserved.
//

import Foundation

func isPrime(number: Int) -> Bool {
    let upper = Int(sqrt(Double(number))) + 1
    if number == 2 {
        return true
    }
    
    for i in 2...upper {
        if number % i == 0 {
            return false
        }
    }
    
    return true
}

var total = 0

for i in 2...2000000 {
    if isPrime(i) {
        print(i)0
        total += i
    }
}

print(total)

