include Math

for b in 1...1000
  for a in 1...b
    c2 = b*b+a*a
    c = sqrt(c2).to_i
    if c*c == c2 and a+b+c == 1000
      print a*b*c
    end
  end
end
