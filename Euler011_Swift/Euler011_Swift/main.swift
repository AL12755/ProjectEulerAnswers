//
//  main.swift
//  Euler011_Swift
//
//  Created by Adam Lyth on 23/06/2016.
//  Copyright © 2016 Adam Lyth. All rights reserved.
//

import Foundation

// Defines the gridsize*gridsize
let gridsize = 20

//Defines the search size
let check = 4

//Reads information from file into 2D array
let filename = "/Users/adamlyth/Dropbox/Projects/ProjectEulerAnswers/Euler011_Swift/data.txt"
let filecontents = try String(contentsOfFile: filename, encoding: NSASCIIStringEncoding)
//Seperate lines by EOL character \n
var lines = filecontents.characters.split{$0 == "\n"}.map(String.init)

//Further seperate lines by spaces
var grid = [[String]]()
for line in lines {
    grid.append(line.characters.split{$0 == " "}.map(String.init))
}



//Calculate greatest product
var greatestProduct = 0
//Horizontal Check
for vertical in 0...(gridsize - 1) {
    for horizontal in 0...(gridsize - check - 1) {
        let horizontalValue = Int(grid[vertical][horizontal])! * Int(grid[vertical][horizontal + 1])! * Int(grid[vertical][horizontal + 2])! * Int(grid[vertical][horizontal + 3])!
        if horizontalValue > greatestProduct {
            greatestProduct = horizontalValue
        }
    }
}

//Vertical Check
for vertical in 0...(gridsize - check - 1) {
    for horizontal in 0...(gridsize - 1) {
        let verticalValue = Int(grid[vertical][horizontal])! * Int(grid[vertical + 1][horizontal])! * Int(grid[vertical + 2][horizontal])! * Int(grid[vertical + 3][horizontal])!
        if verticalValue > greatestProduct {
            greatestProduct = verticalValue
        }
    }
}

//North_West to South_East check
for vertical in 0...(gridsize - check - 1) {
    for horizontal in 0...(gridsize - check - 1) {
        let NWSEValue = Int(grid[vertical][horizontal])! * Int(grid[vertical + 1][horizontal + 1])! * Int(grid[vertical + 2][horizontal + 2])! * Int(grid[vertical + 3][horizontal + 3])!
        if NWSEValue > greatestProduct {
            greatestProduct = NWSEValue
        }
    }
}

//South_West to North_East check
for vertical in check...(gridsize - 1) {
    for horizontal in 0...(gridsize - check - 1) {
        let SWNEValue = Int(grid[vertical][horizontal])! * Int(grid[vertical - 1][horizontal + 1])! * Int(grid[vertical - 2][horizontal + 2])! * Int(grid[vertical - 3][horizontal + 3])!
        if SWNEValue > greatestProduct {
            greatestProduct = SWNEValue
        }
    }
}

print(greatestProduct)
