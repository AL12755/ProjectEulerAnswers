//
//  main.swift
//  Euler012_Swift
//
//  Created by Adam Lyth on 24/06/2016.
//  Copyright © 2016 Adam Lyth. All rights reserved.
//

import Foundation

// Super slow method to find divisors... It might be better to just check prime factors and then multiply them together
// Keeping a list of appropriate prime numbers would be significantly more effort however

func divisors(number: Int) -> Int {
    var div = 1
    let upper = Int(number/2) + 1
    for i in 1...upper {
        if number % i == 0 {
            div += 1
        }
    }
    return(div)
}

var number = 1
var count = 2

while divisors(number) < 500 {
    number += count
    count += 1
}

print(number)

