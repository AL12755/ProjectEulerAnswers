//
//  main.swift
//  Euler004_Swift
//
//  Created by Adam Lyth on 23/06/2016.
//  Copyright © 2016 Adam Lyth. All rights reserved.
//

import Foundation

func isPalindrome(number: Int) -> Bool {
    let stringConversion = String(number)
    
    var reverse = ""
    
    for character in stringConversion.characters {
        let char = "\(character)"
        reverse = char + reverse
    }
    if reverse == stringConversion {
        return true
    } else {
        return false
    }
}

var largestPalindrome = 0

for first in 100...999 {
    for second in 100...999 {
        var product = first*second
        if isPalindrome(product) && product > largestPalindrome {
            largestPalindrome = product
        }
    }
}

print(largestPalindrome)



