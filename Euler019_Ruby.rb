class Date
  @@t30days = [9,4,6,11]
  @@t31days = [1,3,5,7,8,10]
  def initialize
    @day = 1
    @dayname = 0
    @month = 1
    @year = 1900
  end

  def leapyear
    if @year % 100 != 0
      if @year % 4 == 0
        return true
      end
    else
      if @year % 400 == 0
        return true
      end
    end
    return false
  end

  def increment
    @month += 1
    @day = 1
  end

  def advance
    if @@t31days.include? @month and @day == 31
      self.increment
    elsif @@t30days.include? @month and @day == 30
      self.increment
    elsif @month == 2 and self.leapyear and @day == 29
      self.increment
    elsif @month == 2 and !self.leapyear and @day == 28
      self.increment
    elsif @month == 12 and @day == 31
      @month =1
      @day = 1
      @year += 1
    else
      @day += 1
    end
    @dayname += 1
    @dayname = @dayname % 7
  end

  def day
    @day
  end
  def month
    months = ["Jan","Feb","Mar","Apr","May", "Jun", "Jul", "Aug", "Sep", "Nov", "Dec"]
    return months[@month-1]
  end
  def year
    @year
  end
  def date
    return "#{@day}/#{@month}/#{@year}"
  end
  def dayname
    names = ["Mon","Tue","Wed","Thu","Fri","Sat","Sun"]
    return names[@dayname]
  end
  def daynamenum
    return @dayname
  end
end

pb = Date.new
sundays = 0
while pb.year < 2001 do
  if pb.year > 1900 and pb.daynamenum == 6 and pb.day == 1
    sundays += 1
  end
  pb.advance
end

puts sundays
