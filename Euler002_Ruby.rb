class FibonnaciSequence
  @@last = 1
  @@current = 1

  def next
    _temp = @@current
    @@current = @@current + @@last
    @@last = _temp
  end

  def value
    return @@current
  end

end


list = FibonnaciSequence.new
total = 0
while list.value < 4000000 do
    if list.value % 2 == 0
      total += list.value
    end
    list.next
end

puts total
