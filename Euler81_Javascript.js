function node(i,j,val){
  this.i = i;
  this.j = j;
  this.val = val;
  this.right = null;
  this.down = null;
  this.minimumDistance = 0;
  this.minimumDistanceSet = false;
}

function createMatrix() {
  var fs = require('fs');
  var file = fs.readFileSync('Euler81_Matrix.txt', 'utf-8').split(/\n/)
  file.pop()

  var matrix = []
  for (row of file) {
      matrix.push(row.split(",").map(Number))
  }
  return matrix
}

function createGraph() {
  var graph = []
  var matrix = createMatrix();

  for (var i = 0; i < graph.length -1; i++){
    for (var j = 0; j< graph[i].length -1; j++){
      graph[i][j].right = graph[i][j+1];
      graph[i][j].down = graph[i+1][j];
    }
  }

  for (var i = 0; i < graph.length - 1 ; i++){
    graph[i][graph[i].length - 1].down = graph[i+1][graph[i].length - 1]
  }

  for (var j = 0; j < graph[graph.length - 1].length - 1; j++){
    graph[graph.length - 1][j].right = graph[graph.length - 1][j+1];
  }

  graph[graph.length -1][graph[graph.length - 1].length-1].minimumDistance = graph[graph.length -1][graph[graph.length - 1].length-1].val
  graph[graph.length -1][graph[graph.length - 1].length-1].minimumDistanceSet = true;

  return graph
}

function minimumPath(node){
  if (node.minimumDistanceSet == true) {
    return node.minimumDistance;
  }

  if (node.right == null){
    node.minimumDistance = node.val + minimumPath(node.down);
    node.minimumDistanceSet = true;
    return node.minimumDistance;
  } else if (node.down == null) {
    node.minimumDistance = node.val + minimumPath(node.right);
    node.minimumDistanceSet = true;
    return node.minimumDistance;
  } else {
    node.minimumDistance = node.val + Math.min(minimumPath(node.down) , minimumPath(node.right));
    node.minimumDistanceSet = true;
    return node.minimumDistance;
  }
}

var graph = createGraph();
// console.log(graph)
console.log(minimumPath(graph[0][0]))
