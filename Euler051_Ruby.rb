require 'prime'

def primeDigits(i)
  primesDigits1 = Prime.take_while{ |p| p.to_s.length < i}
  primesDigits2 = Prime.take_while{ |p| p.to_s.length < i+1}
  return primesDigits2 - primesDigits1
end

print primeDigits(6)
