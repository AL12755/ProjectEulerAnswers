class CollatzNumber
  def initialize(startingNumber)
    @@startingNumber = startingNumber
    @@value = startingNumber
    @@length = 1
    self.length
  end
  def advance
    if @@value % 2 == 0
      @@value = @@value/2
    else
      @@value = 3*@@value + 1
    end
    @@length += 1
  end
  def length
    while @@value != 1 do
      self.advance
    end
  end
  def returnLength
    return @@length
  end
  def starting
    return @@startingNumber
  end
end



longestchain = 0
longestnumber = 0

for i in 1...1000000
  cN = CollatzNumber.new(i)
  if cN.returnLength > longestchain
    longestchain = cN.returnLength
    longestnumber = cN.starting
  end
end

puts longestnumber
