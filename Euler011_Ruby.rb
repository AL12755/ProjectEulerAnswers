file = "/Users/adamlyth/Dropbox/Projects/ProjectEulerAnswers/Euler011_Swift/data.txt"
fileContents = IO.readlines(file)

grid = Array.new

for line in fileContents
  row = Array.new
  for word in line.split(" ")
    row << word.to_i
  end
  grid << row
end

$gridsize = grid.size
$test123 = 0
$checksize = 4

greatestProduct = 0

#Check Horizontal
for x in 0...$gridsize-$checksize+1
  for y in 0...$gridsize
    size = 1
    for i in 0...$checksize
      size = size * grid[y][x+i]
    end
    if size > greatestProduct
      greatestProduct = size
    end
  end
end

#Check Vertical
for x in 0...$gridsize
  for y in 0...$gridsize-$checksize+1
    size = 1
    for i in 0...$checksize
      size = size * grid[y+i][x]
    end
    if size > greatestProduct
      greatestProduct = size
    end
  end
end

#Check +ve diagonal
for x in 0...$gridsize-$checksize+1
  for y in $checksize-1...$gridsize
    size = 1
    for i in 0...$checksize
      size = size * grid[y-i][x+i]
    end
    if size > greatestProduct
      greatestProduct = size
    end
  end
end

#Check -ve diagonal
for x in 0...$gridsize-$checksize+1
  for y in 0...$gridsize-$checksize+1
    size = 1
    for i in 0...$checksize
      size = size * grid[y+i][x+i]
    end
    if size > greatestProduct
      greatestProduct = size
    end
  end
end

puts greatestProduct
