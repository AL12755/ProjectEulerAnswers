function check(x){
  var digits = []
  for (var i = 1; i< 7 ; i++){
    digits.push((i*x).toString().split(""))
  }

  for (var i=1; i<6; i++){
    var diff = digits[i].filter(x => digits[0].indexOf(x) < 0 );
    if (diff.length > 0) {
      return false
    }
  }
  return true
}

var i = 1

while (check(i) == false){
  i++
}

console.log(i)
