class SumOfTheSquares
  @@value = 0
  def initialize(upper)
    for i in 1...upper+1
      @@value += i*i
    end
  end

  def val
    return @@value
  end

end

class SquareOfTheSum
  @@value = 0
  def initialize(upper)
    for i in 1...upper+1
      @@value += i
    end
    @@value = @@value * @@value
  end

  def val
    return @@value
  end

end

sos1 = SumOfTheSquares.new(100)
sos2 = SquareOfTheSum.new(100)

print sos2.val - sos1.val
