require 'prime'

class ANumbers
  attr_accessor :pair
  def initialize(value)
    @value = value
    self.d
  end

  def value
    @value
  end

  def d
    sum = 0
    for item in proper_divisors
      sum += item
    end
    self.pair = sum
  end

  def counterCheck(counter)
    len = counter.size
    for i in 0...len
      if counter[i] == 0
        return true
      end
    end
    return false
  end

  def incrementCounter(counter)
    len = counter.size
    counter[len-1] += 1
    (len-1).downto(0).each do |i|
      if counter[i] > 1
        counter[i-1] += 1
        counter[i] = 0
      end
    end
  end

  def proper_divisors
    primes = Prime.prime_division(@value)
    set = []
    for prime in primes
      for i in 0...prime[1]
        set << prime[0]
      end
    end
    proper_divisors = [1]

    counter = []
    len = set.length
    for i in 0...len
      counter << 0
    end

    while counterCheck(counter) do
      incrementCounter(counter)
      num = 1
      for i in 0...len
        if counter[i] == 1
          num *= set[i]
        end
      end
      if !proper_divisors.include? num and num != @value
        proper_divisors << num
      end
    end
    return proper_divisors
  end
end

numbersList = Array.new
for i in 1...10000
  numbersList << ANumbers.new(i)
end

count = 0
for item in numbersList
  if item.pair-1 < 10000
    if item.pair == numbersList[item.pair-1].value and numbersList[item.pair-1].pair == item.value and item.value != item.pair
      count += item.value
    end
  end
end

puts count
