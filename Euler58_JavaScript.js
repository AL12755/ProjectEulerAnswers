// var lagrange = require('/Users/adamlyth/Dropbox/Projects/ProjectEulerAnswers/lagrange.js');
// Predict diagonals using Lagrange.
// console.log(lagrange.lagrange([[0,1],[1,3],[2,13]]));
// console.log(lagrange.lagrange([[0,1],[1,5],[2,17]]));
// console.log(lagrange.lagrange([[0,1],[1,7],[2,21]]));
// console.log(lagrange.lagrange([[0,1],[1,9],[2,25]]));


function isPrime(value) {
    for(var i = 2; i <= Math.ceil(Math.sqrt(value)); i++) {
        if(value % i === 0) {
            return false;
        }
    }
    return value > 1;
}

var primes = 0;
var total = 1;
var i = 1
var sideLength = 1

while (i == 1 || primes/total > 0.1){
  var topRight = 4*i*i - 2*i + 1;
  var topLeft = 4*i*i + 1;
  var bottomLeft = 4*i*i + 2*i + 1;
  var bottomRight = 4*i*i + 4*i + 1;

  isPrime(topRight) && primes++;
  isPrime(topLeft) && primes++;
  isPrime(bottomLeft) && primes++;
  isPrime(bottomRight) && primes++;
  total += 4;
  sideLength += 2;
  i++;
}
console.log(sideLength)
