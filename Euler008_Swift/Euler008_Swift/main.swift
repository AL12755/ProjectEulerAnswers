//
//  main.swift
//  Euler008_Swift
//
//  Created by Adam Lyth on 23/06/2016.
//  Copyright © 2016 Adam Lyth. All rights reserved.
//

import Foundation

let filename = "/Users/adamlyth/Dropbox/Projects/ProjectEulerAnswers/Euler008_Swift/grid.txt"
let filecontents = try String(contentsOfFile: filename, encoding: NSASCIIStringEncoding)
let data = filecontents.characters.filter({ Int(String($0)) != nil }).map({ Int(String($0))! })
let searchLength = 13
let size = data.count



var max = 0

for i in 0...(size - 1 - searchLength) {
    var sum = 1
    for j in 0...(searchLength - 1) {
        sum = sum * data[i+j]
    }
    if sum > max {
        max = sum
    }
}

print(max)



